//
//  ShaderManager.m
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 11/09/16.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>
#import "ShaderManager.h"

@implementation ShaderManager
static NSString *StandardShaderPath = @"StandardShader";
static NSMutableDictionary<NSString *, NSNumber *> *ShaderCache;
static GLuint CurrentShader;

+ (void)SetCurrentShader:(GLuint)_shader {
    CurrentShader = _shader;
}

+ (GLuint)GetCurrentShader {
    return CurrentShader;
}

+ (BOOL)LoadStandardShader:(GLuint *)_shader {
    NSNumber * shader = [ShaderCache valueForKey:StandardShaderPath];
    if (shader != nil) {
        *_shader = [shader unsignedIntValue];
        return YES;
    }
    GLuint vertShader, fragShader;
    NSString *vertShaderPathname, *fragShaderPathname;

    // Create shader program.
    *_shader = glCreateProgram();

    // Create and compile vertex shader.
    vertShaderPathname = [[NSBundle mainBundle] pathForResource:@"StandardShader" ofType:@"vsh"];
    if (![ShaderManager CompileShader:&vertShader type:GL_VERTEX_SHADER file:vertShaderPathname]) {
        NSLog(@"Failed to compile vertex shader");
        return NO;
    }

    // Create and compile fragment shader.
    fragShaderPathname = [[NSBundle mainBundle] pathForResource:@"StandardShader" ofType:@"fsh"];
    if (![ShaderManager CompileShader:&fragShader type:GL_FRAGMENT_SHADER file:fragShaderPathname]) {
        NSLog(@"Failed to compile fragment shader");
        return NO;
    }

    // Attach vertex shader to program.
    glAttachShader(*_shader, vertShader);

    // Attach fragment shader to program.
    glAttachShader(*_shader, fragShader);

    // Bind attribute locations.
    // This needs to be done prior to linking.
    glBindAttribLocation(*_shader, GLKVertexAttribPosition, "position");
    glBindAttribLocation(*_shader, GLKVertexAttribTexCoord0, "textureCoords");
    glBindAttribLocation(*_shader, GLKVertexAttribNormal, "normal");

    // Link program.
    if (![self LinkShader:*_shader]) {
        NSLog(@"Failed to link program: %d", *_shader);

        if (vertShader) {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader) {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (*_shader) {
            glDeleteProgram(*_shader);
            *_shader = 0;
        }

        return NO;
    }
    // Release vertex and fragment shaders.
    if (vertShader) {
        glDetachShader(*_shader, vertShader);
        glDeleteShader(vertShader);
    }
    if (fragShader) {
        glDetachShader(*_shader, fragShader);
        glDeleteShader(fragShader);
    }
    NSNumber *number = [[NSNumber alloc] initWithUnsignedInt:(uint)_shader];
    [ShaderCache setObject:number forKey:StandardShaderPath];
    return YES;
}

+ (BOOL)CompileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file {
    GLint status;
    const GLchar *source;

    source = (GLchar *) [[NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil] UTF8String];
    if (!source) {
        NSLog(@"Failed to load vertex shader");
        return NO;
    }

    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);

#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *) malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif

    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0) {
        glDeleteShader(*shader);
        return NO;
    }

    return YES;
}

+ (BOOL)LinkShader:(GLuint)prog {
    GLint status;
    glLinkProgram(prog);

#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *) malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
#endif

    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0) {
        return NO;
    }

    return YES;
}

+ (BOOL)ValidateShader:(GLuint)prog {
    GLint logLength, status;

    glValidateProgram(prog);
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *) malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program validate log:\n%s", log);
        free(log);
    }

    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == 0) {
        return NO;
    }

    return YES;
}

@end
