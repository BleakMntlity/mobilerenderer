//
//  ViewController.h
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 04/09/16.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import "Scene.h"

@interface ViewController : GLKViewController
{
    GLuint vertexBufferId;
    GLuint indexBufferId;
    GLuint textureBufferId;
    GLuint uniformBufferId;
}

@end

