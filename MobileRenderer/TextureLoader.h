//
//  AGLKTextureLoader.h
//  OpenGLES_Ch3_2
//

#import <GLKit/GLKit.h>
#import "TextureInfo.h"

@interface TextureLoader : NSObject

+ (TextureInfo*) CreateTextureFromImage:(CGImageRef) _cgImage PowerOfTwo:(BOOL)_powerOfTwo;
+ (GLuint) CalculatePowerOfTwo:(GLuint)_dimension;

@end
