//
//  GameObject.h
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 06/09/16.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#ifndef GameObject_h
#define GameObject_h

#import "VertexData.h"
#import "TextureInfo.h"
#import "Batch.h"
@import Foundation;

@class Model;
@class Scene;
@class GameObjectDefinition;

@interface GameObject : NSObject

@property GLKVector3 Scale;
@property GLKVector3 Rotation;
@property GLKVector3 Position;
@property GameObject *Parent;
@property NSString* Name;
@property Model* Asset;
@property GLuint Shader;
@property(weak) Scene* ContainingScene;


+ (GameObject *)CreateFromDefinition:(GameObjectDefinition *)_definition FromBatch:(Batch *)_batch;

- (id)init;

- (void)Update;

- (void)Draw;

- (GLKMatrix4)GetModelMatrix;
@end

#endif /* GameObject_h */
