//
//  ModelManager.m
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 24/10/2016.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelManager.h"
#import "GameObject.h"
#import "Model.h"
#import <SceneKit/SCNSceneSource.h>
#import <SceneKit/SCNScene.h>
#import <SceneKit/SCNNode.h>
#import "SCNImporter.h"

@implementation ModelManager

static NSMutableDictionary<NSString *, Batch *> *BatchCache;
static Batch *CurrentBatch;

+ (void)SetCurrentlyBufferedBatch:(Batch *)_batch {
    CurrentBatch = _batch;
}

+ (Batch *)GetCurrentlyBufferedBatch {
    return CurrentBatch;
}

+ (Batch *)LoadBatch:(NSString *)_filePath {
    if (BatchCache == nil) {
        BatchCache = [[NSMutableDictionary alloc] init];
    }

    Batch *batch = [BatchCache valueForKey:_filePath];
    if (batch != nil) {
        return batch;
    }

    NSError *error = [NSError alloc];
    NSData *data = [NSData dataWithContentsOfFile:_filePath options:0 error:&error];
    SCNSceneSource * sceneSrc = [SCNSceneSource sceneSourceWithData:data options:nil];
    return [SCNImporter ImportScn:sceneSrc];
}

+ (TextureInfo *)GetTexture:(NSDictionary *)_data {
    if (_data != nil) {
        TextureInfo *texture = [[TextureInfo alloc] init];
        texture.Height = [[_data valueForKey:@"height"] unsignedIntegerValue];
        texture.Width = [[_data valueForKey:@"width"] unsignedIntegerValue];
        texture.ImageData = [_data valueForKey:@"imageData"];
        return texture;
    }
    return nil;
}

+ (NSMutableDictionary<NSString *, Model *> *)GetModels:(NSDictionary<NSString *, NSDictionary *> *)_data
                                               WithMesh:(Batch *)_mesh {

    NSMutableDictionary<NSString *, Model *> *models = [[NSMutableDictionary alloc] initWithCapacity:_data.count];
    for (NSString *name in _data) {
        NSDictionary *value = [_data valueForKey:name];
        Model *model = [[Model alloc] init];
        model.SharedMesh = _mesh;
        model.Name = [value valueForKey:@"name"];
        model.FirstRenderCommandIndex = [value[@"indexOfFirstCommand"] unsignedIntegerValue];
        model.AmountRenderCommands = [value[@"numberOfCommands"] unsignedIntegerValue];
        NSAssert(model.FirstRenderCommandIndex + model.AmountRenderCommands <= model.SharedMesh.RenderCommands.count, NULL);
        [models setObject:model forKey:model.Name];
    }
    return models;
}

+ (NSArray<RenderCommand *> *)GetCommands:(NSArray <NSDictionary *> *)_data {
    NSUInteger length = _data.count;
    RenderCommand *array[length];
    for (NSUInteger i = 0; i < length; ++i) {
        NSDictionary *dataAti = _data[i];
        RenderCommand *command = [[RenderCommand alloc] init];
        command.FirstIndex = (GLsizei) [dataAti[@"firstIndex"] unsignedIntegerValue];
        command.NumberOfIndices = (GLsizei) [dataAti[@"numberOfIndices"] unsignedIntegerValue];
        command.Mode = (GLenum) [dataAti[@"command"] unsignedIntegerValue];
        array[i] = command;
    }
    return [NSArray arrayWithObjects:array count:length];
}

@end
