//
//  VertexData.h
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 06/09/16.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#ifndef VertexData_h
#define VertexData_h

#import <GLkit/GLKit.h>
typedef struct
{
    GLKVector3 position;
    GLKVector3 normal;
    GLKVector2 texCoords;
} VertexData;



#endif /* VertexData_h */
