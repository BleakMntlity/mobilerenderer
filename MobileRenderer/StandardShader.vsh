attribute vec4 position;
attribute vec3 normal;
attribute vec2 textureCoords;

varying mediump vec2 uvCoords;
varying lowp float vColor;

uniform vec3 lightPosition;
uniform mat4 mvpMatrix;
uniform mat3 normalMatrix;

void main()
{
    vec3 eyeNormal = normalize(normalMatrix * normal);
    uvCoords = vec2(textureCoords[0], textureCoords[1]);
    vColor = max(0.0, dot(eyeNormal, lightPosition));
    vColor = clamp(vColor + 0.3, 0.0, 1.0);
    gl_Position = mvpMatrix * position;
}