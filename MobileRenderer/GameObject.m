//
//  GameObject.m
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 06/09/16.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameObject.h"
#import "ModelManager.h"
#import "Model.h"
#import "ShaderManager.h"
#import "Scene.h"
#import "SceneDefinition.h"
#import "VertexData.h"
#import "TextureAtlas.h"

#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

@implementation GameObject
@synthesize Position;
@synthesize Rotation;
@synthesize Parent;
@synthesize Asset;
@synthesize Shader;
@synthesize ContainingScene;
@synthesize Scale;

+ (GameObject *)CreateFromDefinition:(GameObjectDefinition *)_definition FromBatch:(Batch*)_batch{
    GameObject *go = [[GameObject alloc] init];
    go.Asset = _batch.Models[_definition.Model];
    if (go.Asset == nil) {
        NSLog(@"Model not found");
    }
    go.Name = _definition.Name;
    GLuint shaderId;
    if (![ShaderManager LoadStandardShader:&shaderId]) {
        NSLog(@"couldnt load standard shader");
    } else {
        go.Shader = shaderId;
    }
    return go;
    return nil;
}

- (id)init {
    self = [super init];
    self.Position = GLKVector3Make(0.0f, 0.0f, 0.0f);
    self.Rotation = GLKVector3Make(0.0f, 0.0f, 0.0f);
    self.Scale = GLKVector3Make(0.2f, 0.2f, 0.2f);
    return self;
}

- (void)Update {
    Rotation = GLKVector3Add(Rotation, GLKVector3Make(1, 1, 1));
}

- (void)Draw {
    //TODO also draw children
    if ([ShaderManager GetCurrentShader] != Shader) {
        glUseProgram(Shader);
        [ShaderManager SetCurrentShader:Shader];
    }
    if ([ModelManager GetCurrentlyBufferedBatch] != Asset.SharedMesh) {
        GLsizei size = Asset.SharedMesh.VertexAttributeData.length;
        const void *data = Asset.SharedMesh.VertexAttributeData.bytes;

        GLsizei indexDataSize = Asset.SharedMesh.IndexData.length;
        const void *indexData = Asset.SharedMesh.IndexData.bytes;

        glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
        glEnableVertexAttribArray(GLKVertexAttribPosition);
        glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, Asset.SharedMesh.AttributeSize, NULL + [[Asset.SharedMesh.Offsets objectAtIndex:0] unsignedIntegerValue]);

        glEnableVertexAttribArray(GLKVertexAttribNormal);
        glVertexAttribPointer(GLKVertexAttribNormal, 3, GL_FLOAT, GL_FALSE, Asset.SharedMesh.AttributeSize, NULL + [[Asset.SharedMesh.Offsets objectAtIndex:1] unsignedIntegerValue]);

        glEnableVertexAttribArray(GLKVertexAttribTexCoord0);
        glVertexAttribPointer(GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, Asset.SharedMesh.AttributeSize, NULL + [[Asset.SharedMesh.Offsets objectAtIndex:2] unsignedIntegerValue]);

        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexDataSize, indexData, GL_STATIC_DRAW);
        [ModelManager SetCurrentlyBufferedBatch:Asset.SharedMesh];

        TextureAtlas * texture = Asset.SharedMesh.Texture;
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture.Width, texture.Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, [texture.ImageData bytes]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }

    int texture = glGetUniformLocation(Shader, "texture");
    int mvpMatrix = glGetUniformLocation(Shader, "mvpMatrix");
    int lightPosition = glGetUniformLocation(Shader, "lightPosition");
    int normalMatrix = glGetUniformLocation(Shader, "normalMatrix");

    GLKMatrix4 modelViewMatrix = GLKMatrix4Multiply(ContainingScene.MainCamera.ViewMatrix, [self GetModelMatrix]);
    GLKMatrix4 mvp = GLKMatrix4Multiply(ContainingScene.MainCamera.ProjectionMatrix, modelViewMatrix);
    GLKMatrix3 nMatrix = GLKMatrix4GetMatrix3(GLKMatrix4InvertAndTranspose(modelViewMatrix, NULL));

    glUniform1i(texture, 0);
    glUniformMatrix4fv(mvpMatrix, 1, 0, mvp.m);
    glUniformMatrix3fv(normalMatrix, 1, 0, nMatrix.m);
    GLKVector3 lightPosCameraSpace = GLKMatrix4MultiplyVector3(ContainingScene.MainCamera.ViewMatrix, ContainingScene.LightSource);
    glUniform3f(lightPosition, lightPosCameraSpace.x, lightPosCameraSpace.y, lightPosCameraSpace.z);


    GLenum error = glGetError();
    if (error != GL_NO_ERROR) {
        NSLog(@"GL Error: 0x%x", error);
    }
    NSUInteger indexOfLastCommand = Asset.FirstRenderCommandIndex + Asset.AmountRenderCommands - 1;
    NSAssert(indexOfLastCommand < Asset.SharedMesh.RenderCommands.count, @"index of last command was %i, but has only %i entries", indexOfLastCommand, Asset.SharedMesh.RenderCommands.count);
    for (NSUInteger i = Asset.FirstRenderCommandIndex; i <= indexOfLastCommand; ++i) {
        RenderCommand *command = Asset.SharedMesh.RenderCommands[i];
        glDrawElements(command.Mode, (GLsizei) command.NumberOfIndices, GL_UNSIGNED_INT, ((GLuint *) NULL + command.FirstIndex));
    }
}


- (GLKMatrix4)GetModelMatrix {
    GLKMatrix4 matrix = GLKMatrix4Identity;
    if (Parent != nil) {
        matrix = [Parent GetModelMatrix];
    }
    matrix = GLKMatrix4Rotate(matrix, DEGREES_TO_RADIANS(self.Rotation.x), 1, 0, 0);
    matrix = GLKMatrix4Rotate(matrix, DEGREES_TO_RADIANS(self.Rotation.y), 0, 1, 0);
    matrix = GLKMatrix4Rotate(matrix, DEGREES_TO_RADIANS(self.Rotation.z), 0, 0, 1);
    matrix = GLKMatrix4Translate(matrix, self.Position.x, self.Position.y, self.Position.z);
    matrix = GLKMatrix4Scale(matrix, self.Scale.x, self.Scale.y, self.Scale.z);
    return matrix;
}

@end
