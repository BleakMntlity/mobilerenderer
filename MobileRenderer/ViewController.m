//
//  ViewController.m
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 04/09/16.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#import "ViewController.h"
#import "TextureDebugQuad.h"
#import "Model.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    GLKView *view = (GLKView *) self.view;
    NSAssert([view isKindOfClass:[GLKView class]], @"Invalid view type");
    view.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    [EAGLContext setCurrentContext:view.context];

    glEnable(GL_DEPTH_TEST);
    glClearColor(0, 0, 0, 1);

    glGenBuffers(1, &vertexBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);

    glGenBuffers(1, &indexBufferId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferId);

    glGenTextures(1, &textureBufferId);
    glBindTexture(GL_TEXTURE_2D, textureBufferId);

    [self LoadScene];

    self.preferredFramesPerSecond = 30;
}

- (void)LoadScene {
    Scene *scene = [[Scene alloc] init];
    scene.MainCamera = [[Camera alloc] init];
    scene.MainCamera.ProjectionMatrix = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(65.0f), 9.0f / 16.0f, 0.1f, 200.0f);
    scene.MainCamera.ViewMatrix = GLKMatrix4MakeLookAt(40, 40, 40, 0, 0, 0, 0, 1, 0);
    scene.LightSource = GLKVector3Make(4, 4, -4);
    [scene LoadDefinition];
    Scene.currentScene = scene;
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_CULL_FACE);
    Scene *scene = [Scene getCurrentScene];
    NSUInteger count = scene.GameObjects.count;
    for (NSUInteger i = 0; i < count; ++i) {
        GameObject *current = scene.GameObjects[i];
        [current Update];
        [current Draw];
        //TextureDebugQuad * debug = [[TextureDebugQuad alloc] init:current.Asset.SharedMesh.Texture];
        //[debug Draw];
    }
}

- (void)viewDidUnload {
    GLKView *view = (GLKView *) self.view;
    [EAGLContext setCurrentContext:view.context];

    if (vertexBufferId != 0) {
        glDeleteBuffers(1, &vertexBufferId);
        vertexBufferId = 0;
    }

    if (textureBufferId != 0) {
        glDeleteBuffers(1, &textureBufferId);
        textureBufferId = 0;
    }

    view.context = nil;
    [EAGLContext setCurrentContext:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
