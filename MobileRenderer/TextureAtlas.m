//
//  TextureAtlas.m
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 25/11/2016.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#import "TextureAtlas.h"
#import <math.h>

@implementation MaterialData
@synthesize Texture;
@synthesize Coords;
@synthesize Geometries;
@end

@implementation TextureAtlas
@synthesize STCoords;

-(id)initWithNodes:(NSArray<SCNNode *> *)_nodes {
    self = [super init];
    self.STCoords = [[NSMutableDictionary alloc] init];
    //TODO use image url to avoid duplicate textures
    NSMutableDictionary<SCNMaterial*, MaterialData*>* materials = [[NSMutableDictionary alloc] init];
    for (SCNNode* node in _nodes) {
        SCNMaterial* material;
        UIImage* texture = [self GetTexture:node OutMaterial:&material];
        
        MaterialData* data = [materials objectForKey:material];
        if (data != nil || texture == nil) {
            [data.Geometries addObject:node.geometry];
            continue;
        }
        
        GLsizei currentHeight = CGImageGetHeight(texture.CGImage);
        GLsizei currentWidth = CGImageGetWidth(texture.CGImage);
        data = [[MaterialData alloc] init];
        data.Texture = texture;
        data.Coords = @[@(self.Width), @(self.Height), @(currentWidth + self.Width), @(currentHeight + self.Height)];
        data.Geometries = [[NSMutableArray alloc] initWithObjects:node.geometry, nil];
        [materials setObject:data forKey:material];
        if (self.Height < self.Width) {
            self.Height += currentHeight;
            self.Width = MAX(self.Width, currentWidth);
        } else {
            self.Width += currentWidth;
            self.Height = MAX(self.Height, currentWidth);
        }
    }
    [self CombineTextures:materials];
    [self AdjustSTCoords:materials];
    return self;
}

-(void) AdjustUVsToAtlas:(NSData*)_uvs OfGeometry:(SCNGeometry *)_geometry ComponentSize:(NSInteger)_componentSize{
    NSArray<NSNumber*>* stRange = [self GetRangeForGeometry:_geometry];
    NSParameterAssert(_componentSize == sizeof(float));
    float u = ((float*)_uvs.bytes)[0];
    float v = ((float*)_uvs.bytes)[1];
    float uOffset = [stRange[0] floatValue];
    float vOffset = [stRange[1] floatValue];
    float width = [stRange[2] floatValue] - uOffset;
    float height = [stRange[3] floatValue] - vOffset;
    u = [self Wrap:u Around:width];
    v = [self Wrap:v Around:height];
    ((float*)_uvs.bytes)[0] = u + uOffset;
    ((float*)_uvs.bytes)[1] = v + vOffset;
}

-(NSArray<NSNumber*>*) GetRangeForGeometry:(SCNGeometry*)_geometry {
    for (SCNMaterial* material in self.STCoords) {
        MaterialData* data = [self.STCoords objectForKey:material];
        for (SCNGeometry* geometry in data.Geometries) {
            if (geometry == _geometry) {
                return data.Coords;
            }
        }
    }
    return nil;
}

-(void) CombineTextures:(NSDictionary<SCNMaterial*, MaterialData*>*)_materials {
    float logWidth = log2f(self.Width);
    float logHeight = log2f(self.Height);
    if (logWidth - (int)logWidth > 0.00001) {
        self.Width =  pow(2, (GLuint)(log2f(self.Width) + 1));
    }
    if (logHeight - (int)logHeight > 0.00001) {
        self.Height = pow(2, (GLuint)(log2f(self.Height) + 1));
    }
    // Allocate sufficient storage for RGBA pixel color data with the power of 2 sizes specified
    NSMutableData *imageData = [NSMutableData dataWithLength:self.Height * self.Width * 4];
    // Create a Core Graphics context that draws into the allocated bytes
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef cgContext = CGBitmapContextCreate([imageData mutableBytes], self.Width, self.Height, 8, 4 * self.Width, colorSpace, kCGImageAlphaPremultipliedLast);
    CGColorSpaceRelease(colorSpace);
    // Flip the Core Graphics Y-axis for future drawing
    CGContextTranslateCTM (cgContext, 0, self.Height);
    CGContextScaleCTM (cgContext, 1.0, -1.0);
    
    for (SCNMaterial* material in _materials) {
        MaterialData* data = [_materials objectForKey:material];
        UIImage* texture = data.Texture;
        NSArray<NSNumber*>* coords = data.Coords;
        uint startX = [coords[0] unsignedIntegerValue];
        uint startY = [coords[1] unsignedIntegerValue];
        uint endX = [coords[2] unsignedIntegerValue];
        uint endY = [coords[3] unsignedIntegerValue];
        // Draw the loaded image into the Core Graphics context. Resizing as necessary
        CGContextDrawImage(cgContext, CGRectMake(startX, startY, endX - startX, endY - startY), texture.CGImage);
    }
    CGContextRelease(cgContext);
    self.ImageData = imageData;
}

-(void) AdjustSTCoords:(NSMutableDictionary<SCNMaterial*, MaterialData*>*)_pixelPositions {
    for (SCNMaterial* material in _pixelPositions) {
        MaterialData *data = [_pixelPositions objectForKey:material];
        NSArray<NSNumber*>* coords = data.Coords;
        double startS = [[coords objectAtIndex:0] doubleValue] / (double)self.Width;
        double endS = [[coords objectAtIndex:2] doubleValue] / (double)self.Width;
        double startT = [[coords objectAtIndex:1] doubleValue] / (double)self.Height;
        double endT = [[coords objectAtIndex:3] doubleValue] / (double)self.Height;
        data.Coords = @[@(startS), @(startT), @(endS), @(endT)];
        [self.STCoords setObject:data forKey:material];
    }
}

-(UIImage*) GetTexture:(SCNNode*)_node OutMaterial:(SCNMaterial**)_outMaterial {
    for (SCNMaterial* material in _node.geometry.materials) {
        SCNMaterialProperty* property = (SCNMaterialProperty*)material;
        if (![property.contents isKindOfClass:[NSURL class]]) {
            continue;
        }
        NSURL* fileUrl = (NSURL*)property.contents;
        NSData* imageData = [NSData dataWithContentsOfURL:fileUrl];
        *_outMaterial = material;
        return [UIImage imageWithData:imageData];
    }
    return nil;
}

-(float)Wrap:(float)_value Around:(float)_size {
    if (fmodf(_value, 1.0) == 0) {
        if (_value > 1.0) {
            NSLog(@"Texture too small for triangle. This will look odd with atlassing");
        }
        return _size;
    }
    
    int repeated = abs((int)_value);
    float normalized = _size * _value;
    float wrapped = fmodf(normalized, _size);
    if (_value < 0) {
        wrapped = _size + wrapped;
    }
    if (repeated > 0) {
        NSLog(@"Texture too small for triangle. This will look odd with atlassing");
    }
    return wrapped + repeated;
}

@end
