//
//  TextureAtlas.h
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 25/11/2016.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#ifndef TextureAtlas_h
#define TextureAtlas_h
#import <SceneKit/SCNMaterial.h>
#import "TextureInfo.h"
#import <SceneKit/SCNScene.h>
#import <Foundation/Foundation.h>
#import "SCNImporter.h"
#import "TextureLoader.h"
#import <SceneKit/SCNGeometry.h>

@interface MaterialData : NSObject
@property UIImage* Texture;
@property NSArray<NSNumber*>* Coords;
@property NSMutableArray<SCNGeometry*>* Geometries;
@end

@interface TextureAtlas : TextureInfo
@property NSMutableDictionary<SCNMaterial*, MaterialData*>* STCoords;
-(id)initWithNodes:(NSArray<SCNNode*>*)_nodes;
-(void) AdjustUVsToAtlas:(NSData*)_uvs OfGeometry:(SCNGeometry*)_geometry ComponentSize:(NSInteger)_componentSize;
@end

#endif /* TextureAtlas_h */
