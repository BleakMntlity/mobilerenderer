//
// Created by Fabian Hanselmann on 26/10/2016.
// Copyright (c) 2016 Fabian Hanselmann. All rights reserved.
//

#import "Model.h"
#import "TextureInfo.h"


@implementation Model

@synthesize Name;
@synthesize SharedMesh;
@synthesize FirstRenderCommandIndex;
@synthesize AmountRenderCommands;

-(id)init {
    self = [super init];
    self.FirstRenderCommandIndex = -1;
    return self;
}

- (TextureInfo *)GetTexture {
    return nil;
}


@end
