//
//  ContainingScene.h
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 06/09/16.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#ifndef Scene_h
#define Scene_h

#import <GLKit/GLKit.h>
#import "GameObject.h"
#import "Camera.h"

@interface Scene : NSObject
+(Scene*)getCurrentScene;
+(void)setCurrentScene:(Scene*)scene;
@property (strong, nonatomic) NSMutableArray<GameObject*>* GameObjects;
@property (strong, nonatomic) Camera* MainCamera;
@property GLKVector3 LightSource;

-(void) LoadDefinition;

#endif /* Scene_h */
@end
