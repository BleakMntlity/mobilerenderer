//
//  main.m
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 04/09/16.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
