//
// Created by Fabian Hanselmann on 13/11/2016.
// Copyright (c) 2016 Fabian Hanselmann. All rights reserved.
//

#import "SceneDefinition.h"
#import "GameObject.h"

@implementation GameObjectDefinition
- (id)init:(NSString *)_name WithModel:(NSString *)_model {
    self = [super init];
    self.Name = _name;
    self.Model = _model;
    return self;
}

@end

@implementation SceneDefinition {

@private
    NSArray<GameObjectDefinition *> *_GameObjects;
    NSString *_BatchPath;
}
@synthesize GameObjects = _GameObjects;

@synthesize BatchPath = _BatchPath;

- (id)init {
    self = [super init];
//    self.BatchPath = @"/Users/fabianhanselmann/OpenGLES/MobileRenderer/MobileRenderer/MobileRenderer/Assets/lowpoly/bumper.modelplist";
    self.BatchPath = [[NSBundle mainBundle] pathForResource:@"untitled" ofType:@"scn"];
    self.GameObjects = @[
            [[GameObjectDefinition alloc] init:@"black_pawn1" WithModel:@"SketchUp"],
//            [[GameObjectDefinition alloc] init:@"black_pawn2" WithModel:@"pawn_black"],
//            [[GameObjectDefinition alloc] init:@"black_pawn3" WithModel:@"pawn_black"],
//            [[GameObjectDefinition alloc] init:@"black_pawn4" WithModel:@"pawn_black"],
//            [[GameObjectDefinition alloc] init:@"black_pawn5" WithModel:@"pawn_black"],
//            [[GameObjectDefinition alloc] init:@"black_pawn6" WithModel:@"pawn_black"],
//            [[GameObjectDefinition alloc] init:@"black_pawn7" WithModel:@"pawn_black"],
//            [[GameObjectDefinition alloc] init:@"black_pawn8" WithModel:@"pawn_black"],
//            [[GameObjectDefinition alloc] init:@"white_pawn1" WithModel:@"pawn_white"],
//            [[GameObjectDefinition alloc] init:@"white_pawn2" WithModel:@"pawn_white"],
//            [[GameObjectDefinition alloc] init:@"white_pawn3" WithModel:@"pawn_white"],
//            [[GameObjectDefinition alloc] init:@"white_pawn4" WithModel:@"pawn_white"],
//            [[GameObjectDefinition alloc] init:@"white_pawn5" WithModel:@"pawn_white"],
//            [[GameObjectDefinition alloc] init:@"white_pawn6" WithModel:@"pawn_white"],
//            [[GameObjectDefinition alloc] init:@"white_pawn7" WithModel:@"pawn_white"],
//            [[GameObjectDefinition alloc] init:@"white_pawn8" WithModel:@"pawn_white"],
//            [[GameObjectDefinition alloc] init:@"black_bishop1" WithModel:@"bishop_black"],
//            [[GameObjectDefinition alloc] init:@"black_bishop2" WithModel:@"bishop_black"],
//            [[GameObjectDefinition alloc] init:@"white_bishop1" WithModel:@"bishop_white"],
//            [[GameObjectDefinition alloc] init:@"white_bishop2" WithModel:@"bishop_white"],
//            [[GameObjectDefinition alloc] init:@"black_knight1" WithModel:@"black_knight"],
//            [[GameObjectDefinition alloc] init:@"black_knight2" WithModel:@"black_knight"],
//            [[GameObjectDefinition alloc] init:@"white_knight1" WithModel:@"knight_white"],
//            [[GameObjectDefinition alloc] init:@"white_knight2" WithModel:@"knight_white"],
//            [[GameObjectDefinition alloc] init:@"black_king" WithModel:@"king_black"],
//            [[GameObjectDefinition alloc] init:@"white_king" WithModel:@"king_white"],
//            [[GameObjectDefinition alloc] init:@"black_queen" WithModel:@"queen_black"],
//            [[GameObjectDefinition alloc] init:@"white_queen" WithModel:@"queen_white"],
//            [[GameObjectDefinition alloc] init:@"black_rock" WithModel:@"rock_black"],
//            [[GameObjectDefinition alloc] init:@"white_rock" WithModel:@"rock_white"],
//            [[GameObjectDefinition alloc] init:@"board" WithModel:@"board"]
//            [[GameObjectDefinition alloc] init:@"bumper1" WithModel:@"bumperRinkFloor"],
//            [[GameObjectDefinition alloc] init:@"bumper2" WithModel:@"bumperCar"],
//            [[GameObjectDefinition alloc] init:@"bumper3" WithModel:@"bumperRinkWalls"]
    ];
    return self;
}

@end


