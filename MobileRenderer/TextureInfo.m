//
//  TextureInfo.m
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 11/09/16.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TextureInfo.h"

@implementation TextureInfo

@synthesize ImageData;
@synthesize Width;
@synthesize Height;

-(id)init {
    self = [super init];
    ImageData = [[NSData alloc] init];
    Width = 0;
    Height = 0;
    return self;
}

@end
