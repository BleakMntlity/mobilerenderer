//
//  AGLKTextureLoader.m
//  OpenGLES_Ch3_2
//

#import "TextureLoader.h"
#import <math.h>

@implementation TextureLoader

/////////////////////////////////////////////////////////////////
// This function returns an NSData object that contains bytes
// loaded from the specified Core Graphics image, cgImage. This
// function also returns (by reference) the power of 2 width and
// height to be used when initializing an OpenGL ES texture buffer
// with the bytes in the returned NSData instance. The widthPtr
// and heightPtr arguments must be valid pointers.
+ (TextureInfo*) CreateTextureFromImage:(CGImageRef) _cgImage PowerOfTwo:(BOOL) _powerOfTwo
{
    NSCParameterAssert(NULL != _cgImage);
    
    GLuint originalWidth = (GLuint)CGImageGetWidth(_cgImage);
    GLuint originalHeight = (GLuint)CGImageGetWidth(_cgImage);
    
    NSCAssert(0 < originalWidth, @"Invalid image width");
    NSCAssert(0 < originalHeight, @"Invalid image width");
    
    // Calculate the width and height of the new texture buffer
    // The new texture buffer will have power of 2 dimensions.
    GLuint width = _powerOfTwo ? [TextureLoader CalculatePowerOfTwo:originalWidth] : originalWidth;
    GLuint height = _powerOfTwo ? [TextureLoader CalculatePowerOfTwo:originalHeight] : originalWidth;
    
    // Allocate sufficient storage for RGBA pixel color data with the power of 2 sizes specified
    NSMutableData *imageData = [NSMutableData dataWithLength:height * width * 4];  // 4 bytes per RGBA pixel
    
    NSCAssert(nil != imageData, @"Unable to allocate image storage");
    
    // Create a Core Graphics context that draws into the allocated bytes
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef cgContext = CGBitmapContextCreate([imageData mutableBytes], width, height, 8, 4 * width, colorSpace,kCGImageAlphaPremultipliedLast);
    CGColorSpaceRelease(colorSpace);
    
    // Flip the Core Graphics Y-axis for future drawing
    CGContextTranslateCTM (cgContext, 0, height);
    CGContextScaleCTM (cgContext, 1.0, -1.0);
    
    // Draw the loaded image into the Core Graphics context
    // resizing as necessary
    CGContextDrawImage(cgContext, CGRectMake(0, 0, width, height), _cgImage);
    
    CGContextRelease(cgContext);
    
    TextureInfo* textureInfo = [[TextureInfo alloc] init];
    textureInfo.ImageData = imageData;
    textureInfo.Width = width;
    textureInfo.Height = height;
    
    return textureInfo;
}

/////////////////////////////////////////////////////////////////
// This function calculates and returns the nearest power of 2
// that is greater than or equal to the dimension argument and
// less than or equal to 1024.
+(GLuint) CalculatePowerOfTwo:(GLuint) _dimension
{
    GLuint e = (GLuint)(log2f(_dimension) + 1);
    return pow(2, e);
}

@end
