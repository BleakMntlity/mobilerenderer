//
// Created by Fabian Hanselmann on 26/10/2016.
// Copyright (c) 2016 Fabian Hanselmann. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Batch;
@class RenderCommand;
@class TextureInfo;

@interface Model : NSObject
@property NSString *Name;
@property Batch *SharedMesh;
@property NSUInteger FirstRenderCommandIndex;
@property NSUInteger AmountRenderCommands;

-(id)init;
-(TextureInfo *) GetTexture;
@end
