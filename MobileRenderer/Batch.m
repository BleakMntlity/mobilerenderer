//
//  Mesh.m
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 24/10/2016.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Batch.h"
#import "TextureInfo.h"
#import "Model.h"

@implementation Batch

@synthesize RenderCommands;
@synthesize IndexData;
@synthesize VertexAttributeData;
@synthesize Models;
@synthesize BatchedSources;
@synthesize AttributeSize;
@synthesize IndexSize;
@synthesize Offsets;

- (id)init {
    self = [super init];
    if (self != nil) {
        RenderCommands = [[NSMutableArray alloc] init];
        IndexData = [[NSMutableData alloc] init];
        VertexAttributeData = [[NSMutableData alloc] init];
        Models = [[NSMutableDictionary alloc] init];
        BatchedSources = [[NSMutableDictionary alloc] init];
        Offsets = [[NSMutableArray alloc] initWithArray:@[[[NSNumber alloc] initWithUnsignedInt:0], [[NSNumber alloc] initWithUnsignedInt:12], [[NSNumber alloc]initWithUnsignedInt:24]]];
        AttributeSize = 32;
        IndexSize = 4;
    }
    return self;
}

@end
