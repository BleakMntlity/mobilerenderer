//
//  TextureInfo.h
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 11/09/16.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#ifndef TextureInfo_h
#define TextureInfo_h

#import <OpenGLES/ES3/gl.h>

@interface TextureInfo : NSObject

@property(strong, nonatomic) NSData* ImageData;
@property GLsizei Width;
@property GLsizei Height;

-(id)init;

@end

#endif /* TextureInfo_h */
