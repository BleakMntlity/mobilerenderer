//
// Created by Fabian Hanselmann on 13/11/2016.
// Copyright (c) 2016 Fabian Hanselmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VertexData.h"
#import "TextureInfo.h"
#import "Batch.h"
@import Foundation;

@class GameObject;


@interface GameObjectDefinition : NSObject
@property NSString* Name;
@property GLKVector3* Position;
@property GLKVector3* Rotation;
@property  NSString * Model;

-(id)init:(NSString*)_name WithModel:(NSString *)_model;

@end

@interface SceneDefinition : NSObject
@property NSString * BatchPath;
@property NSArray<GameObjectDefinition*> * GameObjects;

-(id)init;
@end
