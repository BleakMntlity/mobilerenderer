//
//  ShaderManager.h
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 11/09/16.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#ifndef ShaderManager_h
#define ShaderManager_h

@interface ShaderManager : NSObject
+ (void) SetCurrentShader:(GLuint)_shader;
+ (GLuint) GetCurrentShader;
+ (BOOL)LoadStandardShader:(GLuint*)_shader;
+ (BOOL)ValidateShader:(GLuint)prog;
+ (BOOL)CompileShader:(GLuint*)shader type:(GLenum)type file:(NSString *)file;
+ (BOOL)LinkShader:(GLuint)prog;

@end

#endif /* ShaderManager_h */
