//
//  ModelManager.h
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 24/10/2016.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#ifndef ModelManager_h
#define ModelManager_h

#import <objc/NSObject.h>

@class Batch;

@interface ModelManager : NSObject
+ (void)SetCurrentlyBufferedBatch:(Batch*)_batch;
+ (Batch*)GetCurrentlyBufferedBatch;
+ (Batch*)LoadBatch:(NSString *)_filePath;
@end

#endif /* ModelManager_h */
