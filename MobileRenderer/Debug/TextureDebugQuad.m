//
// Created by Fabian Hanselmann on 14/11/2016.
// Copyright (c) 2016 Fabian Hanselmann. All rights reserved.
//

#import "TextureDebugQuad.h"
#import "VertexData.h"
#import "ShaderManager.h"


@implementation TextureDebugQuad {
    GLuint shaderId;
}
@synthesize VertexAmount = _VertexAmount;

@synthesize Vertices = _Vertices;

@synthesize Texture = _Texture;

- (id)init:(TextureInfo *)_texture {
    self = [super init];
    if (self == nil)
        return nil;
    self.Texture = _texture;
    self.Vertices = malloc(6 * sizeof(VertexData));
    self.Vertices[0].position = GLKVector3Make(-1, -1, -1);
    self.Vertices[0].texCoords = GLKVector2Make(0, 0);
    self.Vertices[1].position = GLKVector3Make(1, -1, -1);
    self.Vertices[1].texCoords = GLKVector2Make(1, 0);
    self.Vertices[2].position = GLKVector3Make(-1, 1, -1);
    self.Vertices[2].texCoords = GLKVector2Make(0, 1);
    self.Vertices[0].normal = GLKVector3CrossProduct(self.Vertices[0].position, self.Vertices[1].position);
    self.Vertices[1].normal = GLKVector3CrossProduct(self.Vertices[0].position, self.Vertices[1].position);
    self.Vertices[2].normal = GLKVector3CrossProduct(self.Vertices[0].position, self.Vertices[1].position);

    self.Vertices[3].position = GLKVector3Make(-1, 1, -1);
    self.Vertices[3].texCoords = GLKVector2Make(0, 1);
    self.Vertices[4].position = GLKVector3Make(1, -1, -1);
    self.Vertices[4].texCoords = GLKVector2Make(1, 0);
    self.Vertices[5].position = GLKVector3Make(1, 1, -1);
    self.Vertices[5].texCoords = GLKVector2Make(1, 1);
    self.Vertices[3].normal = GLKVector3CrossProduct(self.Vertices[3].position, self.Vertices[4].position);
    self.Vertices[4].normal = GLKVector3CrossProduct(self.Vertices[3].position, self.Vertices[4].position);
    self.Vertices[5].normal = GLKVector3CrossProduct(self.Vertices[3].position, self.Vertices[4].position);

    [ShaderManager LoadStandardShader:&shaderId];
    self.VertexAmount = 6;
    return self;
}

- (void)Draw {
    glUseProgram(shaderId);
    GLsizei size = self.VertexAmount * sizeof(VertexData);
    const VertexData *data = self.Vertices;

    glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), NULL);

    glEnableVertexAttribArray(GLKVertexAttribNormal);
    glVertexAttribPointer(GLKVertexAttribNormal, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), NULL + offsetof(VertexData, normal));

    glEnableVertexAttribArray(GLKVertexAttribTexCoord0);
    glVertexAttribPointer(GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), NULL + offsetof(VertexData, texCoords));

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, self.Texture.Width, self.Texture.Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, [self.Texture.ImageData bytes]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    int texture = glGetUniformLocation(shaderId, "texture");
    int mvpMatrix = glGetUniformLocation(shaderId, "mvpMatrix");
    int lightPosition = glGetUniformLocation(shaderId, "lightPosition");
    int normalMatrix = glGetUniformLocation(shaderId, "normalMatrix");

    glUniform1i(texture, 0);
    glUniformMatrix4fv(mvpMatrix, 1, 0, GLKMatrix4Identity.m);
    glUniformMatrix3fv(normalMatrix, 1, 0, GLKMatrix3Identity.m);
    glUniform3f(lightPosition, 1, 1, 1);


    GLenum error = glGetError();
    if (error != GL_NO_ERROR) {
        NSLog(@"GL Error: 0x%x", error);
    }

    glDrawArrays(GL_TRIANGLES, 0, self.VertexAmount);
}

- (void)dealloc {
    free(self.Vertices);
}
@end
