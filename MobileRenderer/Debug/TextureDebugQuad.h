//
// Created by Fabian Hanselmann on 14/11/2016.
// Copyright (c) 2016 Fabian Hanselmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameObject.h"


@interface TextureDebugQuad : NSObject
@property TextureInfo * Texture;
@property VertexData * Vertices;
@property int VertexAmount;
- (id)init:(TextureInfo *)_texture;

- (void)Draw;

- (void)dealloc;
@end