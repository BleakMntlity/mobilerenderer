//
// Created by Fabian Hanselmann on 25/10/2016.
// Copyright (c) 2016 Fabian Hanselmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/ES3/gl.h>

@interface RenderCommand : NSObject
@property GLenum Mode;
@property GLsizei FirstIndex;
@property GLsizei NumberOfIndices;
@end