//
//  SCNImporter.m
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 17/11/2016.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCNImporter.h"
#import <SceneKit/SCNGeometry.h>
#import "Model.h"
#import "TextureLoader.h"
#import "TextureAtlas.h"

@implementation SCNImporter

+(Batch*)ImportScns:(NSArray<SCNSceneSource*>*)_sources {
    NSMutableArray<SCNNode*>* allNodes = [[NSMutableArray alloc] init];
    NSMutableDictionary<CopyableNode*,Model*>* allModels = [[NSMutableDictionary alloc] init];
    for (SCNSceneSource* source in _sources) {
        NSError* error;
        SCNScene* scene = [source sceneWithOptions:0 error:&error];
        if (error != nil) {
            NSLog(@"error while scene loading: %@", error.description);
        }
        NSArray<SCNNode*>* currentNodes = [SCNImporter GetNodes:scene];
        for (SCNNode* node in currentNodes) {
            [allNodes addObject:node];
        }
        NSDictionary<CopyableNode*, Model*>* currentModels = [SCNImporter GetModels:scene];
        for (CopyableNode* node in currentModels) {
            [allModels setObject:[currentModels objectForKey:node] forKey:node];
        }
    }
    Batch* batch = [[Batch alloc] init];
    batch.Texture = [[TextureAtlas alloc] initWithNodes:allNodes];
    for (CopyableNode* node in allModels) {
        Model* model = [allModels objectForKey:node];
        model.SharedMesh = batch;
        [batch.Models setObject:model forKey:model.Name];
        [self ExtractGeometry:node.Node OfModel:model Into:batch];
    }
    return batch;
}


+ (Batch*)ImportScn:(SCNSceneSource *)_sceneSource {
    NSError* error;
    SCNScene* scene = [_sceneSource sceneWithOptions:0 error:&error];
    if (error != nil) {
        NSLog(@"error while scene loading: %@", error.description);
    }
    Batch* batch = [[Batch alloc] init];
    batch.Texture = [[TextureAtlas alloc] initWithNodes:[SCNImporter GetNodes:scene]];
    NSDictionary<CopyableNode*, Model*>* models = [SCNImporter GetModels:scene];
    NSAssert(models.count == scene.rootNode.childNodes.count, @"unmatching model count");
    for (CopyableNode* node in models) {
        Model* model = [models  objectForKey:node];
        model.SharedMesh = batch;
        [batch.Models setObject:model forKey:model.Name];
        [self ExtractGeometry:node.Node OfModel:model Into:batch];
    }
    return batch;
}

+(NSArray<SCNNode*>*) GetNodes:(SCNScene*)_scene {
    NSMutableArray<SCNNode*>*nodes = [[NSMutableArray alloc] init];
    [SCNImporter InsertNodesRecursive:_scene.rootNode IntoArray:nodes];
    return nodes;
}

+(void)InsertNodesRecursive:(SCNNode*)_node IntoArray:(NSMutableArray*)_outArray{
    [_outArray addObject:_node];
    for (SCNNode* node in _node.childNodes) {
        [SCNImporter InsertNodesRecursive:node IntoArray:_outArray];
    }
}

+(NSDictionary<CopyableNode*, Model*>*) GetModels:(SCNScene*)_scene {
    NSArray<SCNNode*>* nodes = _scene.rootNode.childNodes;
    NSMutableDictionary<CopyableNode*, Model*>* models = [[NSMutableDictionary alloc] init];
    for (SCNNode* node in nodes) {
        Model* model = [[Model alloc] init];
        model.Name = node.name;
        [models setObject:model forKey:[[CopyableNode alloc] init:node]];
    }
    return models;
}

+ (void) ExtractGeometry:(SCNNode*)_node OfModel:(Model*)_model Into:(Batch*) _batch {
    SCNGeometry* geometry = _node.geometry;
    if (geometry != nil) {
        [SCNImporter AddData:geometry AndModel:_model ToBatch:_batch];
    }
    for (SCNNode* child in _node.childNodes) {
        [self ExtractGeometry:child OfModel:_model Into:_batch];
    }
}

+ (void) AddData:(SCNGeometry*)_geometry AndModel:(Model*)_model ToBatch:(Batch*)_batch {
    NSArray* positionSources = [_geometry geometrySourcesForSemantic:SCNGeometrySourceSemanticVertex];
    NSArray* normalSources = [_geometry geometrySourcesForSemantic:SCNGeometrySourceSemanticNormal];
    NSArray* texCoordsSources = [_geometry geometrySourcesForSemantic:SCNGeometrySourceSemanticTexcoord];
    
    NSParameterAssert(positionSources.count == normalSources.count == texCoordsSources.count == 1);
    
    SCNGeometrySource* positionData = [positionSources objectAtIndex:0];
    SCNGeometrySource* normalData = [normalSources objectAtIndex:0];
    SCNGeometrySource* texCoordsData = [texCoordsSources objectAtIndex:0];
    
    GeometrySource* geometrySource = [[GeometrySource alloc] initWithPositions:positionData Normals:normalData AndUVs:texCoordsData];
    if ([_batch.BatchedSources objectForKey:geometrySource] == nil) {
        NSParameterAssert(positionData.vectorCount == normalData.vectorCount && normalData.vectorCount == texCoordsData.vectorCount);
        
        NSInteger posDataSize = positionData.componentsPerVector * positionData.bytesPerComponent;
        NSInteger normDataSize = normalData.componentsPerVector * normalData.bytesPerComponent;
        NSInteger texDataSize = texCoordsData.componentsPerVector * texCoordsData.bytesPerComponent;
        
        NSParameterAssert(posDataSize == [[_batch.Offsets objectAtIndex:1] unsignedIntegerValue]);
        NSParameterAssert(posDataSize + normDataSize == [[_batch.Offsets objectAtIndex:2] unsignedIntegerValue]);
        NSParameterAssert(posDataSize + normDataSize + texDataSize == _batch.AttributeSize);
        
        NSNumber* offset = [NSNumber numberWithInt:_batch.VertexAttributeData.length / (posDataSize + normDataSize + texDataSize)];
        [_batch.BatchedSources setObject:offset forKey:geometrySource];
        
        for (NSInteger j = 0; j < positionData.vectorCount; ++j) {
            NSData* singlePos = [positionData.data subdataWithRange:NSMakeRange(j * posDataSize, posDataSize)];
            NSData* singleNorm = [normalData.data subdataWithRange:NSMakeRange(j * normDataSize, normDataSize)];
            NSData* singleTexCoord = [texCoordsData.data subdataWithRange:NSMakeRange(j * texDataSize, texDataSize)];
            [_batch.VertexAttributeData appendData:singlePos];
            [_batch.VertexAttributeData appendData:singleNorm];
            [_batch.Texture AdjustUVsToAtlas:singleTexCoord OfGeometry:_geometry ComponentSize:texCoordsData.bytesPerComponent];
            [_batch.VertexAttributeData appendData:singleTexCoord];
        }
    }
    
    NSInteger dataOffset = [SCNImporter GetOffset:geometrySource InBatch:_batch];
    for (NSInteger i = 0; i < _geometry.geometryElementCount; ++i) {
        SCNGeometryElement* geometryElement = [_geometry.geometryElements objectAtIndex:i];
        
        NSInteger offset = _batch.IndexData.length / _batch.IndexSize;
        NSInteger size = geometryElement.bytesPerIndex;
        RenderCommand* renderCommand = [[RenderCommand alloc] init];
        renderCommand.Mode = [SCNImporter Convert:geometryElement.primitiveType];
        renderCommand.FirstIndex = offset;
        _model.FirstRenderCommandIndex = _model.FirstRenderCommandIndex == -1 ? _batch.RenderCommands.count : _model.FirstRenderCommandIndex;
        
        NSData* indexData = [SCNImporter AdjustIndexData:geometryElement.data ForIndexSize:size WithOffset:dataOffset AndTargetSize:_batch.IndexSize];
        NSParameterAssert(geometryElement.data == nil || geometryElement.data.length % size == 0);
        [_batch.IndexData appendData:indexData];
        
        renderCommand.NumberOfIndices = (_batch.IndexData.length / _batch.IndexSize) - renderCommand.FirstIndex;
        ++_model.AmountRenderCommands;
        [_batch.RenderCommands addObject:renderCommand];
    }
}

+ (NSData*) AdjustIndexData:(NSData*)_data ForIndexSize:(NSInteger)_indexSize WithOffset:(NSUInteger)_offset AndTargetSize:(NSInteger)_targetIndexSize {
    NSParameterAssert(_targetIndexSize == sizeof(uint));
    NSInteger targetLength = _data.length * (_targetIndexSize / _indexSize);
    NSMutableData* shiftedIndices = [[NSMutableData alloc] initWithCapacity:targetLength];
    shiftedIndices.length = targetLength;
    if (_indexSize == 1) {
        for (int i = 0; i < _data.length; ++i) {
            uint8_t index = ((uint8_t*)_data.bytes)[i];
            ((uint*)shiftedIndices.bytes)[i] = (uint)(index + _offset);
        }
    }
    else if (_indexSize == 2) {
        NSInteger count = _data.length / _indexSize;
        for (int i = 0; i < count; ++i) {
            uint16_t index = ((uint16_t*)_data.bytes)[i];
            ((uint*)shiftedIndices.bytes)[i] = (uint)(index + _offset);
        }
    } else if (_indexSize == 4) {
        NSInteger count = _data.length / _indexSize;
        for (int i = 0; i < count; ++i) {
            uint index = ((uint*)_data.bytes)[i];
            ((uint*)shiftedIndices.bytes)[i] = (uint)(index + _offset);
        }
    } else {
        NSLog(@"Invalid data format for index data in scn file. Index has %i bytes", _indexSize);
    }
    return shiftedIndices;
}

+ (NSInteger) GetOffset:(GeometrySource*)_geometry InBatch:(Batch*)_batch {
    return [[_batch.BatchedSources objectForKey:_geometry] integerValue];
}

+ (GLenum) Convert:(SCNGeometryPrimitiveType)_type {
    switch (_type) {
        case SCNGeometryPrimitiveTypeTriangles:
            return GL_TRIANGLES;
        case SCNGeometryPrimitiveTypeTriangleStrip:
            return GL_TRIANGLE_STRIP;
        case SCNGeometryPrimitiveTypeLine:
            return GL_LINES;
        case SCNGeometryPrimitiveTypePoint:
            return GL_POINTS;
    }
}

@end
