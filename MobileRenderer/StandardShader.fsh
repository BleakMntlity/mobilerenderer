uniform sampler2D texture;

varying mediump  vec2 uvCoords;
varying lowp float vColor;

precision mediump float;

void main()
{
    vec2 uv = vec2(uvCoords[0], uvCoords[1]);
    gl_FragColor = vColor * texture2D(texture, uv);
}
