//
//  ContainingScene.m
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 06/09/16.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Scene.h"
#import "SceneDefinition.h"
#import "ModelManager.h"

@implementation Scene
@synthesize GameObjects;
@synthesize MainCamera;
@synthesize LightSource;

__strong static Scene *CurrentScene;

+ (Scene *)getCurrentScene {
    return CurrentScene;
}

+ (void)setCurrentScene:(Scene *)scene {
    CurrentScene = scene;
}

- (void)LoadDefinition {
    SceneDefinition * definition = [[SceneDefinition alloc] init];
    Batch * batch = [ModelManager LoadBatch:definition.BatchPath];
    for (GameObjectDefinition * def in definition.GameObjects) {
        GameObject* gameObject = [GameObject CreateFromDefinition:def FromBatch:batch];
        gameObject.ContainingScene = self;
        [self.GameObjects addObject:gameObject];
    }
}


- (id)init {
    self = [super init];
    self.GameObjects = [[NSMutableArray<GameObject *> alloc] init];
    return self;
}

@end
