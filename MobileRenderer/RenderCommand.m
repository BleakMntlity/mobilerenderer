//
// Created by Fabian Hanselmann on 25/10/2016.
// Copyright (c) 2016 Fabian Hanselmann. All rights reserved.
//

#import "RenderCommand.h"


@implementation RenderCommand
@synthesize Mode;
@synthesize FirstIndex;
@synthesize NumberOfIndices;
@end