//
//  Mesh.h
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 24/10/2016.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#ifndef Mesh_h
#define Mesh_h

#import "VertexData.h"
#import "RenderCommand.h"
#import <OpenGLES/ES3/gl.h>
#import <SceneKit/SCNGeometry.h>
#import <Foundation/Foundation.h>
#import "GeometrySource.h"

@class TextureAtlas;
@class Model;

@interface Batch : NSObject

@property TextureAtlas* Texture;
@property NSMutableData * IndexData;
@property NSMutableData * VertexAttributeData;
@property NSMutableArray<RenderCommand*> *RenderCommands;
@property NSMutableDictionary <NSString*, Model* >* Models;
@property NSMutableDictionary<GeometrySource*, NSNumber*>* BatchedSources;
@property NSInteger AttributeSize;
@property NSMutableArray<NSNumber*>* Offsets;
@property NSInteger IndexSize;
//TODO add bounding box
-(id)init;

@end

#endif /* Mesh_h */
