//
//  AppDelegate.h
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 04/09/16.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

