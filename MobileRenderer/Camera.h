//
//  Camera.h
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 23/10/2016.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#ifndef Camera_h
#define Camera_h


#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

@interface Camera : NSObject
@property GLKMatrix4 ViewMatrix;
@property GLKMatrix4 ProjectionMatrix;


- (void)LookAt:(GLKVector3)_lookTarget WithUpVector:(GLKVector3)_up;

#endif /* Camera_h */
@end


