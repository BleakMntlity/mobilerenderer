//
//  SCNImporter.h
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 17/11/2016.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#ifndef SCNImporter_h
#define SCNImporter_h


#import <SceneKit/SCNSceneSource.h>
#import <SceneKit/SCNScene.h>
#import <SceneKit/SCNNode.h>
#import "Batch.h"
#import "GeometrySource.h"


@interface SCNImporter : NSObject
+ (Batch*) ImportScn:(SCNSceneSource*)_sceneSource;
+ (NSArray<SCNNode*>*) GetNodes:(SCNScene*)_scene;
@end

#endif /* SCNImporter_h */
