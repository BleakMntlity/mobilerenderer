//
//  GeometrySource.h
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 21/11/2016.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#ifndef GeometrySource_h
#define GeometrySource_h

#import <Foundation/Foundation.h>
#import <SceneKit/SCNGeometry.h>
#import <SceneKit/SCNNode.h>

@interface CopyableNode : NSObject<NSCopying>
@property SCNNode* Node;
-(id)init:(SCNNode*)_node;
-(id)copyWithZone:(NSZone *)zone;
-(BOOL)isEqual:(id)object;
@end

@interface GeometrySource : NSObject<NSCopying>
@property SCNGeometrySource* PositionData;
@property SCNGeometrySource* NormalData;
@property SCNGeometrySource* UVData;

-(id)initWithPositions:(SCNGeometrySource*)_positions Normals:(SCNGeometrySource*)_normals AndUVs:(SCNGeometrySource*)_uvs;
-(id)copyWithZone:(NSZone *)_zone;
-(BOOL)isEqual:(id)_object;

@end


#endif /* GeometrySource_h */
