//
//  GeometrySource.m
//  MobileRenderer
//
//  Created by Fabian Hanselmann on 21/11/2016.
//  Copyright © 2016 Fabian Hanselmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GeometrySource.h"

@implementation CopyableNode

@synthesize Node;
-(id)init:(SCNNode *)_node {
    self = [super init];
    self.Node = _node;
    return self;
}

-(id)copyWithZone:(NSZone *)zone {
    return [[CopyableNode alloc] init:self.Node];
}

-(BOOL)isEqual:(id)object {
    return [self.Node isEqual:object];
}

@end

@implementation GeometrySource

@synthesize PositionData;
@synthesize NormalData;
@synthesize UVData;

-(id)initWithPositions:(SCNGeometrySource *)_positions Normals:(SCNGeometrySource *)_normals AndUVs:(SCNGeometrySource *)_uvs{
    self = [super init];
    self.PositionData = _positions;
    self.NormalData = _normals;
    self.UVData = _uvs;
    return self;
}

-(id)copyWithZone:(NSZone *)_zone {
    return [[GeometrySource alloc] initWithPositions:self.PositionData Normals:self.NormalData AndUVs:self.UVData];
}

-(BOOL)isEqual:(id)_object {
    if ([_object isKindOfClass:[GeometrySource class]]) {
        GeometrySource* asGeometrySource = (GeometrySource*)_object;
        return [self.PositionData isEqual:asGeometrySource.PositionData] && [self.NormalData isEqual:asGeometrySource.NormalData] && [self.UVData isEqual:asGeometrySource.UVData];
    }
    return NO;
}

@end

